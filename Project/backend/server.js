const express = require('express')
const app = express()

app.get('/players', function (req, res) {
    res.json(
        [
            { id: 1, name: 'Ronaldo', number: 7 },
            { id: 2, name: 'Modric', number: 10 },
            { id: 3, name: 'Mo Salah', number: 12 }
        ]
    )
})

app.post('/players', function (req, res) {
    res.send('Got a POST request')
})

app.put('/user', function (req, res) {
    res.send('Got a PUT request at /user')
})

app.delete('/user', function (req, res) {
    res.send('Got a DELETE request at /user')
})

app.listen(3000, () => console.log('Example app listening on port 3000!'))