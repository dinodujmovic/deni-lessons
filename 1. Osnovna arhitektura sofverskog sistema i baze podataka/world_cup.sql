/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : world_cup

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 09/06/2018 23:04:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for countries
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES (1, 'Croatia');
INSERT INTO `countries` VALUES (2, 'Portugal');
INSERT INTO `countries` VALUES (3, 'Germany');
INSERT INTO `countries` VALUES (4, 'Spain');
INSERT INTO `countries` VALUES (5, 'France');
INSERT INTO `countries` VALUES (6, 'England');
INSERT INTO `countries` VALUES (7, 'Iceland');
INSERT INTO `countries` VALUES (8, 'Egypt');
INSERT INTO `countries` VALUES (9, 'Serbia');
INSERT INTO `countries` VALUES (10, 'Senegal');

-- ----------------------------
-- Table structure for players
-- ----------------------------
DROP TABLE IF EXISTS `players`;
CREATE TABLE `players`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `number` int(11) NULL DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `country_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `team_id`(`country_id`) USING BTREE,
  CONSTRAINT `players_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of players
-- ----------------------------
INSERT INTO `players` VALUES (1, 'Cristiano', 'Ronaldo Dos Santos', '1985-01-06', 7, 'CR7', 2);
INSERT INTO `players` VALUES (2, 'Luka', 'Modric', '1985-07-06', 10, 'Luka', 1);
INSERT INTO `players` VALUES (3, 'Mohamed', 'Salah', '1992-07-06', 10, 'Mo', 8);
INSERT INTO `players` VALUES (4, 'Ivan', 'Rakitic', '1987-06-06', 7, 'Raketa', 1);
INSERT INTO `players` VALUES (5, 'Marco', 'Asensio', '1996-11-06', 20, 'Marko', 4);
INSERT INTO `players` VALUES (6, 'Thomas', 'Muler', '1990-06-09', 11, 'Miler', 3);

SET FOREIGN_KEY_CHECKS = 1;
