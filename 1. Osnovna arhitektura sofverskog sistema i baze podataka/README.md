### Najpoznatije baze
* MySQL - najlaksa za uciti, ima jednostavni autoincrement
* PostgreSQL
* Oracle
* Microsoft Server - vecinom se koristi u kombinaciji sa .NET jezikom za Web servise (Windows)
* SQLite - SQL je offline baza, da bi se koristila ne zahtjeva instalaciju database servera. Primjer koristenja: 
    1. Skine se neki database file
    2. Kopira se u odredjeni folder mobilne aplikacije
    3. Preko neke biblioteke pristupa se .sql file-u (datoteci) i izvrsavaju se SQL upiti

### Programi pomocu kojih se mogu koristiti sve baze (MySQL, SQLite... itd..)
* Navicat
* AquaFold


-------------------------
##### Koristeni pojmovi:

JSON:  
https://www.w3schools.com/js/js_json_syntax.asp  
https://www.w3schools.com/js/js_json_objects.asp

Spajanje tabela u bazi podataka (JOIN):  
https://www.w3schools.com/sql/sql_join.asp  

Pojam REST-a (cisto informativno - na sta se misli kada se kaze REST, suprotnost od REST-a je SOAP):  
https://stackoverflow.com/questions/671118/what-exactly-is-restful-programming  

SQL jezik (Dovoljno je proci i procitati i probati ovaj tutorial da se nauce OSNOVE! SQL jezika):  
https://www.w3schools.com/sql/  

-------------------------
##### Problemi pri lekciji:

MySQL Workbench ima bug, te se nije mogao konektovati na MySQL server.
Sve je radilo uz Navicat program.
