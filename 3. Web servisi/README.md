#### Web servisi

Programiranje web servisa (jezici):  

* Java (Java EE [Enterprise Edition], Java Spring..)  
* .NET (C#)  
* NodeJS (JavaScript) https://nodejs.org/en/download/    
    * Instaliranjem NodeJS, instalira Node package manager (npm) za dohvatanje biblioteka sa github.com, i komanda 'node' koja je zapravo sluzi za pokretanje node servera  
    * Biblioteka za REST servise https://expressjs.com/  

